\contentsline {chapter}{\numberline {第1章\hspace {.3em}}绪论}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}研究背景}{1}{section.1.1}% 
\contentsline {subsection}{\numberline {1.1.1}验证码的定义}{1}{subsection.1.1.1}% 
\contentsline {subsection}{\numberline {1.1.2}验证码的分类及其识别技术的发展}{1}{subsection.1.1.2}% 
\contentsline {section}{\numberline {1.2}本文工作}{2}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}主要工作}{2}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}本文内容}{3}{subsection.1.2.2}% 
\contentsline {chapter}{\numberline {第2章\hspace {.3em}}可分割验证码的识别}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}验证码图像预处理}{5}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}图像数字化}{5}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}灰化}{5}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}去噪}{6}{subsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.1.4}字符分割}{7}{subsection.2.1.4}% 
\contentsline {subsection}{\numberline {2.1.5}二值化}{7}{subsection.2.1.5}% 
\contentsline {section}{\numberline {2.2}验证码特征提取}{7}{section.2.2}% 
\contentsline {section}{\numberline {2.3}验证码的识别}{8}{section.2.3}% 
\contentsline {section}{\numberline {2.4}识别结果}{8}{section.2.4}% 
\contentsline {chapter}{\numberline {第3章\hspace {.3em}}端到端的验证码识别}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}TensorFlow深度学习框架简介}{9}{section.3.1}% 
\contentsline {section}{\numberline {3.2}卷积神经网络模型搭建}{10}{section.3.2}% 
\contentsline {section}{\numberline {3.3}GPU模型训练与识别}{11}{section.3.3}% 
\contentsline {section}{\numberline {3.4}识别结果}{11}{section.3.4}% 
\contentsline {chapter}{\numberline {第4章\hspace {.3em}}结论}{13}{chapter.4}% 
\contentsline {section}{\numberline {4.1}结论}{13}{section.4.1}% 
\contentsline {section}{\numberline {4.2}进一步的工作}{13}{section.4.2}% 
\contentsline {chapter}{参考文献}{15}{section*.17}% 
\contentsline {chapter}{致谢}{17}{chapter*.19}% 
